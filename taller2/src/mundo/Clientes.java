package mundo;

import java.util.Date;

public class Clientes extends Trabajo
{
	public enum tiposDeClientes
	{
		UNIVERSIDAD, COLEGIO, TRABAJO, OTROS;
	}

	protected tiposDeClientes elTipo;
	
	public Clientes(Date pFecha, String pLugar, boolean pObligatorio, boolean pFormal, tiposDeTrabajo pTipo, tiposDeClientes pTipoC) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		elTipo = pTipoC;
	}

	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}

	public String toString()
	{
			return "Evento con Clientes: \n"
			  		+"FECHA: " + this.fecha
			  		+"\nLUGAR: " + this.lugar
			  		+"\nTIPO EVENTO: " + this.tipoDeTrabajo
			  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
			  		+"\nFORMAL: "+ conv(this.formal);
	}
}
