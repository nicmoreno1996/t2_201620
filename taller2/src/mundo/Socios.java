package mundo;

import java.util.Date;

public class Socios extends Trabajo
{
	public enum tiposDeSocios
	{
		UNIVERSIDAD, COLEGIO, TRABAJO, OTROS;
	}

	protected tiposDeSocios elTipo;
	
	public Socios(Date pFecha, String pLugar, boolean pObligatorio, boolean pFormal, tiposDeTrabajo pTipo, tiposDeSocios pTipoS) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		elTipo = pTipoS;
	}

	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
			return "Evento con Socios: \n"
			  		+"FECHA: " + this.fecha
			  		+"\nLUGAR: " + this.lugar
			  		+"\nTIPO EVENTO: " + this.tipoDeTrabajo
			  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
			  		+"\nFORMAL: "+ conv(this.formal);
	}
}
