package mundo;

import java.util.Date;

public class Trabajo extends Evento
{
	public enum tiposDeTrabajo
	{
		JUNTAS, ALMUERZOS, CENAS, COCTELES, PRESENTACIONES, OTROS;
	}
	
	protected tiposDeTrabajo tipoDeTrabajo;

	public Trabajo(Date pFecha, String pLugar, boolean pObligatorio, boolean pFormal, tiposDeTrabajo pTipo) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipoDeTrabajo = pTipo;
	}
	
}
